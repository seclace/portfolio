import PropTypes from 'prop-types'

export const navigationShape = PropTypes.shape({
  dispatch: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  navigate: PropTypes.func.isRequired,
  setParams: PropTypes.func.isRequired,
  state: PropTypes.shape({
    key: PropTypes.string,
    routeName: PropTypes.string,
    params: PropTypes.object,
  }),
})
