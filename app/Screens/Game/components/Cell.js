import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import { Entypo } from '@expo/vector-icons'

class Cell extends Component {
  static propTypes = {
    position: PropTypes.string,
    type: PropTypes.oneOf([ 'cross', 'circle', '' ]),
    onFill: PropTypes.func.isRequired,
  }

  render () {
    const { position, type, onFill } = this.props
    const Container = type && position ? View : TouchableOpacity
    return (
      <Container
        onPress={() => onFill(position, Math.random() > 0.5 ? 'circle' : 'cross')}
        style={[styles.crossContainer, this.positionStyle]}>
        { type ? <Entypo name={type} size={this.size} /> : null }
      </Container>
    )
  }

  get size () {
    const { type } = this.props
    return type === 'cross' ? 46 : 36
  }

  get positionStyle () {
    const { position } = this.props
    switch (position) {
      case 'top-left': return styles.topLeft
      case 'top-center': return styles.topCenter
      case 'top-right': return styles.topRight
      case 'middle-left': return styles.middleLeft
      case 'middle-center': return styles.middleCenter
      case 'middle-right': return styles.middleRight
      case 'bottom-left': return styles.bottomLeft
      case 'bottom-center': return styles.bottomCenter
      case 'bottom-right': return styles.bottomRight
      default: return styles.noBorder
    }
  }
}

const styles = StyleSheet.create({
  crossContainer: {
    width: 70,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
  },
  noBorder: {
    borderWidth: 0,
  },
  topLeft: {
    borderTopWidth: 0,
    borderLeftWidth: 0,
  },
  topCenter: {
    borderTopWidth: 0,
  },
  topRight: {
    borderTopWidth: 0,
    borderRightWidth: 0,
  },
  middleLeft: {
    borderLeftWidth: 0,
  },
  middleRight: {
    borderRightWidth: 0,
  },
  bottomLeft: {
    borderBottomWidth: 0,
    borderLeftWidth: 0,
  },
  bottomCenter: {
    borderBottomWidth: 0,
  },
  bottomRight: {
    borderBottomWidth: 0,
    borderRightWidth: 0,
  },
})

export default Cell
