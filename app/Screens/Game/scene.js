import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity, Text } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'

import Cell from './components/Cell'
import styles from './style'

const initialGameState = {
  filledCells: {
    'top-left': '',
    'top-center': '',
    'top-right': '',
    'middle-left': '',
    'middle-center': '',
    'middle-right': '',
    'bottom-left': '',
    'bottom-center': '',
    'bottom-right': '',
  },
  playerCellType: '',
  computerCellType: '',
  winner: '',
  movesHistory: [],
}

// TODO: please, refactor it!
export default class Game extends React.Component {

  static propTypes = {
    gamePassed: PropTypes.bool.isRequired,
    actions: PropTypes.shape({
      passTheGame: PropTypes.func.isRequired,
    }),
  }

  constructor (props) {
    super(props)
    this.state = initialGameState

    this.resetGameState = this.resetGameState.bind(this)
    this.getCellByPosition = this.getCellByPosition.bind(this)
    this.fillCellWithType = this.fillCellWithType.bind(this)
    this.fillCellByPlayer = this.fillCellByPlayer.bind(this)
    this.fillCellByComputer = this.fillCellByComputer.bind(this)
    this.makeComputerMove = this.makeComputerMove.bind(this)
    this.checkResults = this.checkResults.bind(this)
  }

  render () {
    const { playerCellType } = this.state

    return (
      <View style={styles.container}>
        {playerCellType ? this.resetButton : null}
        {
          playerCellType
            ? this.board
            : this.playerTypeChooser
        }
      </View>
    )
  }

  get resetButton () {
    return (
      <TouchableOpacity
        style={styles.resetButton}
        onPress={this.resetGameState}>
        <MaterialCommunityIcons name={'restart'} size={36}/>
        <Text style={styles.resetButtonText}>Restart the game</Text>
      </TouchableOpacity>
    )
  }

  get board () {
    return (
      <View style={styles.board}>
        <View style={styles.row}>
          {this.getCellByPosition('top-left')}
          {this.getCellByPosition('top-center')}
          {this.getCellByPosition('top-right')}
        </View>
        <View style={styles.row}>
          {this.getCellByPosition('middle-left')}
          {this.getCellByPosition('middle-center')}
          {this.getCellByPosition('middle-right')}
        </View>
        <View style={styles.row}>
          {this.getCellByPosition('bottom-left')}
          {this.getCellByPosition('bottom-center')}
          {this.getCellByPosition('bottom-right')}
        </View>
      </View>
    )
  }

  get playerTypeChooser () {
    return (
      <View style={styles.playerTypeChooser}>
        <View>
          <Text style={styles.playerTypeChooserHeader}>Choose your marker</Text>
        </View>
        <View style={styles.playerTypeChooserBody}>
          <Cell type={'cross'} onFill={() => this.setState(
            { playerCellType: 'cross', computerCellType: 'circle' })}/>
          <View style={styles.spacerBetweenTypes}/>
          <Cell type={'circle'} onFill={() => this.setState(
            { playerCellType: 'circle', computerCellType: 'cross' })}/>
        </View>
      </View>
    )
  }

  get playerFilledCells () {
    const { filledCells, playerCellType } = this.state
    return Object.keys(filledCells).
      filter(cellPos => filledCells[cellPos] === playerCellType)
  }

  get computerFilledCells () {
    const { filledCells, computerCellType } = this.state
    return Object.keys(filledCells).
      filter(cellPos => filledCells[cellPos] === computerCellType)
  }

  get filledCellsLength () {
    return this.playerFilledCells.length + this.computerFilledCells.length
  }

  getCellByPosition (cellPosition) {
    const { filledCells } = this.state

    return <Cell
      onFill={this.fillCellByPlayer}
      position={cellPosition}
      type={filledCells[cellPosition]}/>
  }

  async fillCellByPlayer (cellPosition) {
    const { playerCellType, computerCellType } = this.state
    const movesMade = this.playerFilledCells.length +
      this.computerFilledCells.length
    const type = movesMade % 2 === 0 ? playerCellType : computerCellType
    await this.fillCellWithType(cellPosition, type)
    this.makeComputerMove()
  }

  // TODO: need to be finished the AI
  makeComputerMove () {
    const { playerFilledCells, state } = this
    const { filledCells, movesHistory } = state
    const emptyCells = Object.keys(filledCells).filter(k => !filledCells[k])
    this.fillCellByComputer(getRandom(emptyCells))
    // switch (playerFilledCells.length) {
    //   case 1:
    //     this.handleFirstPlayerMove(playerFilledCells[0])
    //     break
    //   case 2:
    //     if (needBlockPlayer(playerFilledCells).blockNeed) {}
    //     else if (!filledCells['middle-center'] &&
    //       !movesHistory[0].match(/middle|center/ig)) this.fillCellByComputer(
    //       'middle-center')
    //     else if (1) {}
    // }
  }

  fillCellByComputer (cellPosition) {
    const { computerCellType } = this.state
    this.fillCellWithType(cellPosition, computerCellType)
  }

  fillCellWithType (cellPosition, cellType) {
    return new Promise(resolve => {
      const { winner } = this.state
      if (!winner) {
        this.setState(
          ({ filledCells, movesHistory }) => ({
            filledCells: {
              ...filledCells,
              [cellPosition]: cellType,
            },
            movesHistory: [...movesHistory, cellPosition],
          }),
          () => this.checkResults(resolve),
        )
      }
    })
  }

  resetGameState () {
    this.setState(initialGameState)
  }

  handleFirstPlayerMove (playerMove) {
    if (cellInCorner(playerMove)) {
      let cellPosition = getRandom(playerMove.split('-'))
      if (['top', 'bottom'].includes(cellPosition)) {
        cellPosition = `${cellPosition}-center`
      } else if (['left', 'right'].includes(cellPosition)) {
        cellPosition = `middle-${cellPosition}`
      }
      this.fillCellByComputer(cellPosition)
    } else if (cellInMiddleCenter(playerMove)) {
      const cellPosition = `${getRandom(['top', 'bottom'])}-${getRandom(
        ['left', 'right'])}`
      this.fillCellByComputer(cellPosition)
    } else {
      this.fillCellByComputer('middle-center')
    }
  }

  checkResults (callback) {
    const handleCallback = () => typeof callback === 'function' && callback()
    if (checkWinnerByFilledCells(this.playerFilledCells)) {
      alert('You won!')
      this.setState({ winner: 'player' }, handleCallback)
      this.props.actions.passTheGame()
    } else if (checkWinnerByFilledCells(this.computerFilledCells)) {
      alert('Computer won!')
      this.setState({ winner: 'computer' }, handleCallback)
    } else if (this.filledCellsLength === 9) {
      alert('Round draw')
      this.setState({ winner: 'friendship' }, handleCallback)
    } else {
      handleCallback()
    }
  }
}

const topLeft_bottomRight = ['top-left', 'middle-center', 'bottom-right']
const bottomLeft_topRight = ['top-right', 'middle-center', 'bottom-left']

function cellInCorner (cellPosition) {
  return ['top-left', 'top-right', 'bottom-left', 'bottom-right'].includes(
    cellPosition)
}

function cellInMiddleCenter (cellPosition) {
  return 'middle-center' === cellPosition
}

function checkWinnerByFilledCells (filledCells = []) {
  return filledCells.filter(
    cellPos => topLeft_bottomRight.includes(cellPos)).length === 3 ||
    filledCells.filter(
      cellPos => bottomLeft_topRight.includes(cellPos)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/center/)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/right/)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/left/)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/middle/)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/bottom/)).length === 3 ||
    filledCells.filter(cellPos => cellPos.match(/top/)).length === 3
}

function getRandom (input = []) {
  return input.splice(Math.round(Math.random() * (input.length - 1)), 1)[0]
}

function needBlockPlayer (filledCells) {
  const variants = [
    filledCells.filter(cellPos => topLeft_bottomRight.includes(cellPos)),
    filledCells.filter(cellPos => bottomLeft_topRight.includes(cellPos)),
    filledCells.filter(cellPos => cellPos.match(/center/)),
    filledCells.filter(cellPos => cellPos.match(/right/)),
    filledCells.filter(cellPos => cellPos.match(/left/)),
    filledCells.filter(cellPos => cellPos.match(/middle/)),
    filledCells.filter(cellPos => cellPos.match(/bottom/)),
    filledCells.filter(cellPos => cellPos.match(/top/)),
  ]

  const variant = variants.filter(v => v.length === 2).length && variants.filter(v => v.length === 2)[0]

  const cell = 1

  return {
    blockNeed: !!variant,
    blockCell: cell,
  }
}
