import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  resetButton: {
    paddingVertical: 20,
    marginBottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
  },
  resetButtonText: {
    fontSize: 22,
    paddingLeft: 20,
  },
  playerTypeChooserBody: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  spacerBetweenTypes: {
    width: 30,
  },
  playerTypeChooserHeader: {
    fontSize: 22,
  },
})
