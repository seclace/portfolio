import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Entypo } from '@expo/vector-icons'

import GameScene from './scene'
import * as appActions from 'Modules/Store/app/actions'
import { selectGamePassed } from 'Modules/Store/app/selectors'
import { white } from 'Theme/colors'

function GameContainer (props) {

  function mapStateToProps (state) {
    return {
      gamePassed: selectGamePassed(state),
    }
  }

  function mapDispatchToProps (dispatch) {
    return {
      actions: bindActionCreators({ ...appActions }, dispatch)
    }
  }

  const Scene = connect(mapStateToProps, mapDispatchToProps)(GameScene)
  return <Scene {...props} />
}

GameContainer.navigationOptions = {
  drawerLabel: 'The Game',
  drawerIcon: () => (
    <Entypo name={'game-controller'} size={24} style={{ color: white }}/>
  ),
}

export default GameContainer
