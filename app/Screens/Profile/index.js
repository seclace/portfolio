import React from 'react'
import { StyleSheet, Text, ScrollView } from 'react-native'
import { Feather } from '@expo/vector-icons'

import { white } from 'Theme/colors'

export default class Profile extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Profile',
    drawerIcon: () => (
      <Feather name={'user'} size={24} style={{ color: white }} />
    ),
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Text>Profile here</Text>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
