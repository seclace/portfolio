import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, ScrollView, TouchableWithoutFeedback } from 'react-native'
import { Feather } from '@expo/vector-icons'
import * as Animatable from 'react-native-animatable'

import { navigationShape } from 'PropTypes'

export default class Home extends React.Component {

  static propTypes = {
    gamePassed: PropTypes.bool.isRequired,
    navigation: navigationShape,
  }

  constructor (props) {
    super(props)
    this.state = {
      pressed: false,
      pressedInner: false,
    }
  }

  render () {
    const { gamePassed } = this.props

    return (
      <ScrollView contentContainerStyle={styles.container}>
        {
          gamePassed
            ? this.passedGameInner
            : this.notPassedGameInner()
        }
      </ScrollView>
    )
  }

  get passedGameInner () {
    return (
      <View>
        <Text>Game already passed!</Text>
        {this.notPassedGameInner('Retry?')}
      </View>
    )
  }

  notPassedGameInner = (startText = 'Start') => {
    const { pressed, pressedInner } = this.state

    return (
      <TouchableWithoutFeedback
        onPressIn={() => this.togglePressed(true)}
        onPressOut={() => this.togglePressed(false)}
      >
        <Animatable.View
          ref='outer'
          style={{
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,.2)',
            shadowColor: '#000000',
            shadowOffset: { width: pressed || pressedInner ? -3 : -10, height: pressed || pressedInner ? 3 : 10 },
            shadowRadius: 2,
            shadowOpacity: .3,
            backgroundColor: '#fc4e53',
            paddingRight: 150,
            paddingTop: 50,
            paddingBottom: 20,
            paddingLeft: 20,
          }}
        >
          <TouchableWithoutFeedback
            onPressIn={() => this.togglePressedInner(true)}
            onPressOut={() => this.togglePressedInner(false)}
          >
            <Animatable.View
              ref='inner'
              style={{
                borderWidth: 1,
                borderColor: 'rgba(0,0,0,.2)',
                shadowColor: '#000000',
                shadowOffset: { width: pressedInner ? -2 : -5, height: pressedInner ? 2 : 5 },
                shadowRadius: 2,
                shadowOpacity: .3,
                backgroundColor: '#fbc633',
                justifyContent: 'center',
                padding: 10,
              }}>
              <Text>{startText}</Text>
            </Animatable.View>
          </TouchableWithoutFeedback>
        </Animatable.View>
      </TouchableWithoutFeedback>
    )
  }

  togglePressed = pressed => {
    this.setState({ pressed })
  }

  togglePressedInner = async pressedInner => {
    this.setState({ pressedInner })
    if (pressedInner) {
      await this.refs.outer.swing(400)
      this.props.navigation.navigate('Game')
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
