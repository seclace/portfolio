import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Feather } from '@expo/vector-icons'

import HomeScene from './scene'
import { selectGamePassed } from 'Modules/Store/app/selectors'
import { white } from 'Theme/colors'

function HomeContainer (props) {

  function mapStateToProps (state) {
    return {
      gamePassed: selectGamePassed(state),
    }
  }

  function mapDispatchToProps (dispatch) {
    return {
      actions: bindActionCreators({}, dispatch)
    }
  }

  const Scene = connect(mapStateToProps, mapDispatchToProps)(HomeScene)
  return <Scene {...props} />
}

HomeContainer.navigationOptions = {
  drawerLabel: 'Home',
  drawerIcon: () => (
    <Feather name={'home'} size={24} style={{ color: white }}/>
  ),
}

export default HomeContainer
