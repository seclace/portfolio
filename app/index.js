import React from 'react'
import { Provider } from 'react-redux'

import Navigator from 'Modules/Navigator'
import configureStore from 'Modules/Store'

const store = configureStore()

export default () => {
  return (
    <Provider store={store}>
      <Navigator/>
    </Provider>
  )
}
