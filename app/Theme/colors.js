export const white = '#ffffff'
export const activeDark = '#5824dd'
export const activeLight = '#2ac69b'

export const themeColors = {
  'fc4e53':'#fc4e53',
  '2ac0e4':'#2ac0e4',
  'fbc633':'#fbc633',
  'ff9c31':'#ff9c31',
  'f889b1':'#f889b1',
  'fb6b9b':'#fb6b9b',
  'a363b4':'#a363b4',
  'efeeee':'#efeeee',
  '363636':'#363636',
}
