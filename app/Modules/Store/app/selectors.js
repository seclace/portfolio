import { createSelector } from 'reselect'

const getGamePassed = state => state.app.gamePassed

export const selectGamePassed = createSelector(
  [getGamePassed],
  gamePassed => gamePassed
)
