import { PASS_THE_GAME } from './types'

export function passTheGame () {
  return {
    type: PASS_THE_GAME,
  }
}
