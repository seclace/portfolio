import { PASS_THE_GAME } from 'Modules/Store/app/types'

const initialState = {
  gamePassed: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case PASS_THE_GAME:
      return {
        ...state,
        gamePassed: true,
      }
    default:
      return state
  }
}
