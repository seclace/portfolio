import { Platform } from 'react-native'
import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'

import rootReducer from './rootReducer'
import actionCreators from './actionCreators'

const middlewares = [thunk]
const reducer = combineReducers(rootReducer)
let composeEnhancers = compose
let enhancer = composeEnhancers(applyMiddleware(...middlewares))

if (__DEV__) {
  const loggerMiddleware = createLogger()
  composeEnhancers = (
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
    require('remote-redux-devtools').composeWithDevTools
  )({
    name: Platform.OS,
    ...require('../../../package').remotedev,
    realtime: true,
    actionCreators,
  })

  enhancer = composeEnhancers(
    applyMiddleware(...middlewares, loggerMiddleware),
  )
}

export default function configureStore (initialState) {
  const store = createStore(reducer, initialState, enhancer)
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = combineReducers(require('./rootReducer').default)
      store.replaceReducer(nextRootReducer)
    })
  }
  return store
}
