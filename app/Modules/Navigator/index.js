import { DrawerNavigator } from 'react-navigation'

import Home from 'Screens/Home'
import Profile from 'Screens/Profile'
import Game from 'Screens/Game'

import { activeDark, activeLight, white } from 'Theme/colors'

const drawerOptions = {
  initialRouteName: 'Home',
  drawerBackgroundColor: activeDark,
  contentOptions: {
    activeTintColor: activeLight,
    inactiveTintColor: white,
    itemsContainerStyle: {
      marginVertical: 25,
    },
  },
}

const Navigator = new DrawerNavigator({
  Home: {
    screen: Home,
  },
  Profile: {
    screen: Profile,
  },
  Game: {
    screen: Game,
  },
}, drawerOptions)

export default Navigator
